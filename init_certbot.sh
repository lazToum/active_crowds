#!/bin/bash
# credits:
# https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71
if ! [[ -x "$(command -v docker-compose)" ]]; then
  echo 'Error: docker-compose is not installed ?. :(' >&2
  exit 1
fi
HERE=$(cd -P -- "$(dirname -- "${0}")" && pwd -P)
if [[ -f "${HERE}/.env" ]];then
    . "${HERE}/.env"
else
  echo "Error: File not found: ${HERE}/.env"
  echo "Copy ${HERE}/env.template to ${HERE}/.env modify it and retry"
   >&2
  exit 1
fi
DOMAIN_NAME=${DOMAIN_NAME:-example.com}
CERTBOT_EMAIL=${CERTBOT_EMAIL:-""}
staging=0 # Set to 1 if you're testing your setup to avoid hitting request limits

mkdir -p "${HERE}/nginx/conf.d"
mkdir -p "${HERE}/nginx/certbot/conf"
mkdir -p "${HERE}/nginx/certbot/www"

function make_conf() {
cat >"${HERE}/nginx/conf.d/app.conf"<<EOL
server {
    listen 80;
    server_name ${DOMAIN_NAME};
    server_tokens off;

    location /.well-known/acme-challenge/ {
      root /var/www/certbot;
    }

    location / {
        return 301 https://\$host\$request_uri;
    }
}


server {
    root /app/static/;
    server_tokens off;
    server_name ${DOMAIN_NAME};
    charset utf-8;
    listen [::]:443 ssl ipv6only=on;
    listen 443 ssl;
    ssl_certificate /etc/letsencrypt/live/${DOMAIN_NAME}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/${DOMAIN_NAME}/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
    client_max_body_size 50M;
    client_body_buffer_size 50M;
    client_body_timeout 60;

    location /static {
        root /app/;
    }

    location / {
        proxy_pass http://flask:8000;
        proxy_set_header X-Forwarded-Proto https;
        proxy_set_header X-Url-Scheme \$scheme;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_redirect off;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-Host \$server_name;
    }
}

EOL
}



if [[ ! -f "${HERE}/nginx/conf.d/app.conf" ]]; then
    make_conf
fi

function set_domain() {
    if [[ -f "${HERE}/nginx/conf.d/app.conf" ]]; then
        if [[ "`uname`" == 'Darwin' ]]; then
           sed -i '' 's|.*X-Forwarded-Host.*|X-Forwarded-Host;|' "${HERE}/nginx/conf.d/app.conf";
           sed -i '' 's|.*server_name.*|    server_name '${DOMAIN_NAME}';|' "${HERE}/nginx/conf.d/app.conf";
           sed -i '' 's|.*X-Forwarded-Host.*|        proxy_set_header X-Forwarded-Host \$server_name;|' "${HERE}/nginx/conf.d/app.conf";
           sed -i '' 's|\(.*\)etc/letsencrypt/live/.*/\(.*\)|\1etc/letsencrypt/live/'${DOMAIN_NAME}'/\2|' "${HERE}/nginx/conf.d/app.conf";
        else
           sed -i 's|.*X-Forwarded-Host.*|X-Forwarded-Host;|' "${HERE}/nginx/conf.d/app.conf";
           sed -i 's|.*server_name.*|    server_name '${DOMAIN_NAME}';|' "${HERE}/nginx/conf.d/app.conf";
           sed -i 's|.*X-Forwarded-Host.*|        proxy_set_header X-Forwarded-Host \$server_name;|' "${HERE}/nginx/conf.d/app.conf";
           sed -i 's|\(.*\)etc/letsencrypt/live/.*/\(.*\)|\1etc/letsencrypt/live/'${DOMAIN_NAME}'/\2|' "${HERE}/nginx/conf.d/app.conf";
        fi
    fi
}

set_domain

rsa_key_size=4096
data_path="${HERE}/nginx/certbot"

if [[ -d "$data_path/conf/live/${DOMAIN_NAME}" ]]; then
  read -p "Existing data found for ${DOMAIN_NAME}. Continue and replace existing certificate? (y/N) " decision
  if [[ "$decision" != "Y" ]] && [[ "$decision" != "y" ]]; then
    exit
  fi
fi


if [[ ! -e "$data_path/conf/options-ssl-nginx.conf" ]] || [[ ! -e "$data_path/conf/ssl-dhparams.pem" ]]; then
  mkdir -p "$data_path/conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/tls_configs/options-ssl-nginx.conf > "$data_path/conf/options-ssl-nginx.conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/ssl-dhparams.pem > "$data_path/conf/ssl-dhparams.pem"
fi

path="/etc/letsencrypt/live/${DOMAIN_NAME}"
mkdir -p "$data_path/conf/live/${DOMAIN_NAME}"
docker-compose run --rm --entrypoint "\
    openssl req -nodes -x509 -newkey rsa:1024 -days 1 \
    -keyout ${path}/privkey.pem \
    -out ${path}/fullchain.pem \
    -subj '/CN=localhost'" certbot

docker-compose up --force-recreate -d nginx
docker-compose run --rm --entrypoint "\
  rm -Rf /etc/letsencrypt/live/${DOMAIN_NAME} && \
  rm -Rf /etc/letsencrypt/archive/${DOMAIN_NAME} && \
  rm -Rf /etc/letsencrypt/renewal/${DOMAIN_NAME}.conf" certbot

#Join ${DOMAIN_NAME}s to -d args
domain_args="-d ${DOMAIN_NAME}"

# Select appropriate email arg
case "${CERTBOT_EMAIL}" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email ${CERTBOT_EMAIL}" ;;
esac

# Enable staging mode if needed
if [[ ${staging} != "0" ]]; then staging_arg="--staging"; fi

docker-compose run --rm --entrypoint "\
  certbot certonly --webroot --webroot-path /var/www/certbot \
    $staging_arg \
    $email_arg \
    ${domain_args} \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal \
    --no-eff-email" certbot

make_conf
set_domain
docker-compose exec nginx nginx -s reload
