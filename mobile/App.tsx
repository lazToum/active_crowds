/*
 */
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './src/HomeScreen';
import RunTaskScreen from './src/RunTaskScreen';
import SubCategoriesScreen from './src/SubCategoriesScreen';
import CategoriesScreen from './src/CategoriesScreen';
import TaskDetailsScreen from './src/TaskDetailsScreen';

export const AC_FINISHED_TASKS = 'AC_FINISHED_TASKS';
export const AC_TOKEN = 'AC_TOKEN';

const AppNavigator = createStackNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    Category: {
        screen: CategoriesScreen,
        navigationOptions: ({ navigation }) => {
            const name = navigation.getParam('category').name;
            const title = `${name[0].toUpperCase()}${name.slice(1)}`;
            return ({
                title: `${title} Tasks`
            })},
    },
    SubCategory: {
        screen: SubCategoriesScreen,
        navigationOptions: ({ navigation }) => {
            const name = navigation.getParam('subcategory').description;
            const title = `${name[0].toUpperCase()}${name.slice(1)}`;
            return ({
            title
        })},
    },
    TaskDetails: {
        screen: TaskDetailsScreen,
        navigationOptions: ({ navigation }) => ({
            title: `${ navigation.getParam('task').title }`
        }),
    },
    RunTask: {
        screen: RunTaskScreen,
        navigationOptions: ({ navigation }) => ({
            title: `${ navigation.getParam('task').title }`
        }),
    },
},{
    initialRouteName: 'Home',
});


export default createAppContainer(AppNavigator);

