import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    StatusBar,
    Dimensions,
} from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import uuid from 'uuid';
import { ApiUrl, mainColor } from '../Config';
import { Category, Task } from './Models';
import { calcTileDimensions, Tile } from './Tile';
const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: mainColor,
    },
    body: {
        backgroundColor: mainColor,
        height: Dimensions.get('window').height,
        // minHeight:  calcTileDimensions(width, 2).size * 2,
        justifyContent: "flex-start",
        flexDirection: "row",
        flexWrap: "wrap",
        marginTop: 30,
    },
});

type Props = {
    navigation: NavigationStackProp;
};

const SubCategoriesScreen: React.FC<Props> = ({ navigation }) => {
    const tasks: Task[] = navigation.getParam('tasks');
    const category: Category = navigation.getParam('category');
    const subcategory: Category = navigation.getParam('subcategory');
    const { navigate } = navigation;
    const tileDimensions = calcTileDimensions(width, 2);
    return (
        <>
            <StatusBar barStyle={'dark-content'} backgroundColor={mainColor} />
            <SafeAreaView>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}>
                    <View style={styles.body}>
                        {
                            tasks.map((task: Task) => (
                                <Tile
                                    key={uuid.v4().toString()}
                                    size={tileDimensions.size}
                                    margin={tileDimensions.margin}
                                    title={task.title}
                                    imgSrc={`${ApiUrl}/${task.cover}`}
                                    onTap={() => {
                                        navigate('TaskDetails', {task, category, subcategory});
                                        return true;
                                    }}
                                />
                            ))}
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
};

export default SubCategoriesScreen;
