import React from "react";
import { GestureResponderEvent, Image, StyleSheet, Text, View } from "react-native";
import * as uuid from "uuid";

type tileProps = {
  size: number;
  margin: number;
  title: string;
  onTap: (event: GestureResponderEvent) => boolean;
  imgSrc: string;
};


export const Tile: React.FC<tileProps> = ({size, margin, imgSrc, title, onTap}) => {
    return (
        <View
            onStartShouldSetResponder={onTap}
            key={uuid.v4()}
            style={{width: size, height: size + 50, marginHorizontal: margin, marginBottom: 10}}
        >
            <Image
                style={{ maxWidth: size, maxHeight: size, width: size, height: size, marginBottom: 5}}
                source={{uri: imgSrc}}
            />
            <Text style={styles.itemText}>{title}</Text>
        </View>
    )
};

export const calcTileDimensions = (deviceWidth : number, tpr: number) => {
    const margin = deviceWidth / (tpr * 10);
    const size = (deviceWidth - margin * (tpr * 2)) / tpr;
    return { size, margin };
};

const styles = StyleSheet.create({
    itemText: {
        fontSize: 20,
        textAlign: 'center'
    },
});
