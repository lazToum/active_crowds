import React from 'react';
import {
    Text,
    View,
    Dimensions,
    Animated,
    PanResponder,
    PanResponderInstance,
    Button, ActivityIndicator, TouchableHighlight, SafeAreaView,
} from 'react-native';
// @ts-ignore
import { createImageProgress } from 'react-native-image-progress';
import { Task } from './Models';
import { ApiUrl } from '../Config';
import FastImage from 'react-native-fast-image'
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;
const SWIPE_THRESHOLD = 0.25 * SCREEN_WIDTH;
type SwipeProps = {
    task: Task;
    resources: string[];
    onSubmit: (answers: {[key: string]: string}) => void;
};

type SwipeState = {
    currentIndex: number;
    answers: {[key: string]: string}
    loaded:{[key: string]: boolean}
}

const Image = createImageProgress(FastImage);

export default class Swipe extends React.Component<SwipeProps, SwipeState> {
    private position: Animated.ValueXY;
    private readonly rotate: Animated.AnimatedInterpolation;
    private rotateAndTranslate: { transform: ({ rotate: Animated.AnimatedInterpolation } | { [p: string]: Animated.AnimatedValue })[] };
    private readonly likeOpacity: Animated.AnimatedInterpolation;
    private readonly dislikeOpacity: Animated.AnimatedInterpolation;
    private PanResponder: PanResponderInstance;
    private nextCardOpacity: Animated.AnimatedInterpolation;
    private nextCardScale: Animated.AnimatedInterpolation;

    constructor(props: SwipeProps) {
        super(props);

        this.position = new Animated.ValueXY();
        this.state = {
            currentIndex: 0,
            answers: {},
            loaded: {},
        };
        FastImage.preload(this.props.resources.map(resource => ({uri: `${ApiUrl}/${resource}`})));
        this.rotate = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH /2 ,0, SCREEN_WIDTH /2],
            outputRange: ['-30deg', '0deg', '10deg'],
            extrapolate: 'clamp'
        });

        this.rotateAndTranslate = {
            transform: [{
                rotate: this.rotate
            },
                ...this.position.getTranslateTransform()
            ]
        };

        this.likeOpacity = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: [0, 0, 1],
            extrapolate: 'clamp'
        });
        this.dislikeOpacity = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: [1, 0, 0],
            extrapolate: 'clamp'
        });

        this.nextCardOpacity = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: [1, 0, 1],
            extrapolate: 'clamp'
        });
        this.nextCardScale = this.position.x.interpolate({
            inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
            outputRange: [1, 0.8, 1],
            extrapolate: 'clamp'
        });
        this.PanResponder = PanResponder.create({

            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (evt, gestureState) => {

                this.position.setValue({ x: gestureState.dx, y: gestureState.dy })
            },
            onPanResponderRelease: (evt, gestureState) => {
                if (gestureState.dx > SWIPE_THRESHOLD) {
                    Animated.spring(this.position, {
                        toValue: { x: SCREEN_WIDTH + SWIPE_THRESHOLD, y: gestureState.dy }
                    }).start(() => {
                        const {answers, currentIndex} = this.state;
                        answers[this.props.resources[currentIndex]] = this.props.task.class_names[0];
                        this.setState({ currentIndex: currentIndex + 1, answers }, () => {
                            this.position.setValue({ x: 0, y: 0 })
                        })
                    })
                }
                else if (gestureState.dx < -SWIPE_THRESHOLD) {
                    Animated.spring(this.position, {
                        toValue: { x: -SCREEN_WIDTH - 2 * SWIPE_THRESHOLD, y: gestureState.dy }
                    }).start(() => {
                        const {answers, currentIndex} = this.state;
                        answers[this.props.resources[currentIndex]] = this.props.task.class_names[1];
                        this.setState({ currentIndex: currentIndex + 1, answers }, () => {
                            this.position.setValue({ x: 0, y: 0 })
                        })
                    })
                }
                else {
                    Animated.spring(this.position, {
                        toValue: { x: 0, y: 0 },
                        friction: 4
                    }).start()
                }
            }
        })
    }

    renderResource = (resource: string, active: boolean) => {
        // if (active) {
            // console.log(resource);
        // }
        return (
            <View style={ {
                flex: 1,
                backgroundColor: 'white',
                width: SCREEN_WIDTH,
                height: SCREEN_HEIGHT - 120,
                paddingHorizontal: 10,
                margin: 0,
                position: 'absolute'
            } }>
                <Animated.View style={ {
                    opacity: active ? this.likeOpacity : 0,
                    transform: [{ rotate: '-30deg' }],
                    position: 'absolute',
                    top: 50,
                    left: 40,
                    zIndex: 1000
                } }>
                    <Text style={ {
                        borderWidth: 1,
                        borderColor: 'green',
                        color: 'green',
                        fontSize: 32,
                        fontWeight: '800',
                        padding: 10
                    } }>YES</Text>

                </Animated.View>

                <Animated.View style={ {
                    opacity: active ? this.dislikeOpacity : 0,
                    transform: [{ rotate: '30deg' }],
                    position: 'absolute',
                    top: 50,
                    right: 40,
                    zIndex: 1000
                } }>
                    <Text style={ {
                        borderWidth: 1,
                        borderColor: 'red',
                        color: 'red',
                        fontSize: 32,
                        fontWeight: '800',
                        padding: 10
                    } }>NO</Text>

                </Animated.View>
                <View style={ { flex: 1, borderRadius: 20, backgroundColor: '#00000000' } }>
                    <Image
                        style={ { flex: 1 } }
                        source={ {
                            uri: `${ ApiUrl }/${ resource }`,
                            priority: FastImage.priority.normal,
                        } }
                        resizeMode={ FastImage.resizeMode.contain }
                        indicator={() => <ActivityIndicator />}
                    />
                </View>

            </View>
        );
    };

    renderCards = () => {

        const { resources } = this.props;
        return resources.map((resource, i) => {

            if (i !== this.state.currentIndex) {
                return  null;
            }
            return (
                <Animated.View
                    {...this.PanResponder.panHandlers}
                    key={resource} style={[this.rotateAndTranslate, { height: SCREEN_HEIGHT - 120, width: SCREEN_WIDTH, padding: 10, position: 'absolute' }]}
                >
                    {this.renderResource(resource, i === this.state.currentIndex)}
                </Animated.View>
            )
        }).reverse();
    };

    render() {
        return this.state.currentIndex < this.props.resources.length ? (
            <View style={{ flex: 1 }}>
                <View style={{ height: 20 }} />
                <View style={{ flex: 1 }}>
                     {this.renderCards()}
                </View>
                <View style={{ height: 20 }} />
            </View>
        ) : this.props.resources.length > 0 ? (
            <View style={{ flexGrow: 1, height: '100%', justifyContent: 'flex-end' }}>
                <Button
                    title="Submit"
                    onPress={() => {this.props.onSubmit(this.state.answers)}}
                />
            </View>
        ): null;
    }
}