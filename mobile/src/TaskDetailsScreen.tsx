import React from 'react';
import {
    Button,
    Dimensions,
    Image,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { ApiUrl, mainColor } from '../Config';
import { Category, Task } from './Models';
const { height } = Dimensions.get('window');
type Props = {
    navigation: NavigationStackProp;
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: mainColor,
        flexGrow: 1
    },
    body: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    taskCover: {
        marginLeft: 'auto',
        marginRight: 'auto',
        marginBottom: 10,
        marginTop: 10,
        width: height / 2,
        height: height / 2
    }
});

const TaskDetailsScreen: React.FC<Props> = ({ navigation }) => {
    const task: Task = navigation.getParam('task');
    const category: Category = navigation.getParam('category');
    const subcategory: Category = navigation.getParam('subcategory');
    const { navigate } = navigation;
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}
            >
                <View style={styles.body}>
                    <Image
                        style={styles.taskCover}
                        source={{uri: `${ApiUrl}/${task.cover}`}}
                    />
                    <Text style={{ textAlign: 'center' }}>{task.description}</Text>
                </View>
            </ScrollView>
            <View style={{ flexGrow: 0 }}>
                <Button
                    title="Let's Go!"
                    onPress={() => navigate('RunTask', {task, category, subcategory})}
                />
            </View>
        </SafeAreaView>
    )
};

export default TaskDetailsScreen;
