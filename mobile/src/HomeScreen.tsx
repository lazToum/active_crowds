import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Dimensions,
} from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import uuid from 'uuid';
import { AC_FINISHED_TASKS, AC_TOKEN } from '../App';
import { mainColor, ApiUrl, ClientId, ClientSecret } from '../Config';
import { Category } from './Models';
import { calcTileDimensions, Tile } from './Tile';
import axios, { AxiosResponse } from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { CategoryType, TaskCategoryType } from './types';
const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: mainColor,
    },
    body: {
        backgroundColor: mainColor,
        height: Dimensions.get('window').height,
        // minHeight:  calcTileDimensions(width, 2).size * 2,
        justifyContent: "flex-start",
        flexDirection: "row",
        flexWrap: "wrap",
        marginTop: 30,
    },
});
// import { Category, Task } from './types';
const { width } = Dimensions.get('window');

type Props = {
    navigation: NavigationStackProp;
};

export const getToken = async () => {
    try {
        const stored = await AsyncStorage.getItem(AC_TOKEN);
        if (stored !== undefined && stored !== null) {
            return JSON.parse(stored);
        } else {
            const response: AxiosResponse = await axios.post(`${ApiUrl}/oauth/token`, {client_id: ClientId, client_secret: ClientSecret});
            if (response.status < 400) {
                await AsyncStorage.setItem(AC_TOKEN, JSON.stringify(response.data));
                return response.data;
            } else {
                return undefined;
            }
        }
    } catch (e) {
        console.log(e);
        return undefined;
    }
};

const HomeScreen: React.FC<Props> = ({navigation}) => {
    const { navigate } = navigation;
    const tileDimensions = calcTileDimensions(width, 2);
    const [categories, setCategories] = useState<Category[]>([]);
    useEffect(() => {
        // AsyncStorage.removeItem(AC_FINISHED_TASKS).then(null);
        getToken().then(token => {
            axios.get(`${ApiUrl}/api/categories`, { headers: {Authorization: `Bearer ${token.access_token}`} }).then((response: AxiosResponse) => {
                if (response.status < 400) {
                    const taskCategories: Category[] = [];
                    const _categories = response.data as TaskCategoryType[];
                    _categories.forEach((_category: TaskCategoryType) => {
                        const category = Category.fromJSON(_category);
                        if (category !== null) {
                            taskCategories.push(category);
                        }
                    });
                    setCategories(taskCategories);

                }
            }).catch(reason => {
                console.log(reason);
            });
        });
    }, []);
    return (
        <>
            <StatusBar barStyle={'dark-content'} backgroundColor={mainColor} />
            <SafeAreaView>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}>
                    <View style={styles.body}>
                        {
                            categories.map((category: Category) => {
                                const name = category.name.toString();
                                const title = `${name[0].toUpperCase()}${name.slice(1)}`;
                                return (
                                    <Tile
                                        key={uuid.v4().toString()}
                                        size={tileDimensions.size}
                                        margin={tileDimensions.margin}
                                        title={title}
                                        imgSrc={`${ApiUrl}/${category.cover}`}
                                        onTap={() => {
                                            if (category.subcategories.length > 0) {
                                                navigate('Category', { category });
                                            }
                                            return true;
                                        }}
                                    />
                                )
                            })
                        }
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
};

export default HomeScreen;
