import AsyncStorage from '@react-native-community/async-storage';
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView, View,
} from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { AC_FINISHED_TASKS } from '../App';
import { ApiUrl } from '../Config';
import { getStoredTasks } from './CategoriesScreen';
import { getToken } from './HomeScreen';
import { Category, Task } from './Models';
import axios, { AxiosResponse } from 'axios';
import Swipe from './Swipe';
type Props = {
    navigation: NavigationStackProp;
};

const RunTaskScreen: React.FC<Props> = ({ navigation }) => {
    const task: Task = navigation.getParam('task');
    const category: Category = navigation.getParam('category');
    const subcategory: Category = navigation.getParam('subcategory');
    const [taskResources, setTaskResources] = useState([]);
    useEffect(() => {
        getToken().then(token => {
            axios.get(`${ApiUrl}/api/tasks/${task.id}/resources`, {headers: {Authorization: `Bearer ${token.access_token}`}}).then((response: AxiosResponse) => {
                if (response.status < 400) {
                    setTaskResources(response.data);
                }
            }).catch(reason => {
                console.log(reason);
                navigation.goBack();
            })
        });
    },[]);
    return (
        <SafeAreaView>
            <View style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
            }}
            >
                <Swipe task={task} resources={taskResources} onSubmit={
                    answers => {
                        getToken().then(token => {
                            axios.post(`${ApiUrl}/api/tasks/${task.id}/values`, answers, {headers: { Authorization: `Bearer ${token.access_token}` }}).then((response: AxiosResponse) => {
                                getStoredTasks(tasks => {
                                    tasks[task.id] = task.resources_id;
                                    AsyncStorage.setItem(AC_FINISHED_TASKS, JSON.stringify(tasks)).then(() => {
                                        navigation.navigate('Home');
                                    }).catch(reason => {
                                        console.log(reason);
                                        navigation.navigate('Home');
                                    })
                                });
                            }).catch(reason => {
                                console.log(reason);
                                navigation.navigate('Home');
                            });
                        });
                    }
                } />
            </View>
        </SafeAreaView>
    )
};

export default RunTaskScreen;
