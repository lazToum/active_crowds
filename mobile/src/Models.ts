import { CategoryType, TaskType, TaskStatus, SubcategoryType, TaskCategoryType, TaskSubcategoryType } from './types';

export class Task implements TaskType {
    id: string;
    resources_id: string;
    title: string;
    cover: string;
    description: string;
    class_names: string[];
    status: TaskStatus;
    category: CategoryType;
    subcategory: SubcategoryType;
    constructor(id: string, resources_id: string, title: string, status: TaskStatus, cover: string, description: string, category: CategoryType, subcategory: SubcategoryType, class_names: string[]) {
        this.id = id;
        this.resources_id = resources_id;
        this.title = title;
        this.cover = cover;
        this.description = description;
        this.status = status;
        this.category = category;
        this.subcategory = subcategory;
        this.class_names = class_names;
    }

    toJSON(): TaskType {
        return Object.assign({}, this);
    }

    static fromJSON(json: TaskType) : Task {
        let task = Object.create(Task.prototype);
        return Object.assign(task, json);
    }
}

export class SubCategory implements TaskSubcategoryType {
    name: SubcategoryType;
    description: string;
    cover: string;

    constructor(name: SubcategoryType, description: string, cover: string) {
        this.name = name;
        this.description = description;
        this.cover = cover;
    }

    toJSON(): TaskSubcategoryType {
        return Object.assign({}, this);
    }

    static fromJSON(json: TaskSubcategoryType) : SubCategory {
        let subcategory = Object.create(SubCategory.prototype);
        return Object.assign(subcategory, json);
    }
}

export class Category implements TaskCategoryType {
    name: CategoryType;
    description: string;
    cover: string;
    subcategories: TaskSubcategoryType[];
    constructor(name: CategoryType, description: string, cover: string, subcategories: TaskSubcategoryType[]) {
        this.name = name;
        this.description = description;
        this.cover = cover;
        this.subcategories = subcategories;
    }

    toJSON(): TaskCategoryType {
        return Object.assign({}, this);
    }

    static fromJSON(json: TaskCategoryType): Category {
        let category = Object.create(Category.prototype);
        return Object.assign(category, json, {
            subcategories: json.subcategories
                .map((_subcategory: TaskSubcategoryType) => SubCategory.fromJSON(_subcategory)),
        });
    }
}