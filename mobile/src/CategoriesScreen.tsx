import AsyncStorage from '@react-native-community/async-storage';
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    StatusBar,
    Dimensions,
} from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import uuid from 'uuid';
import { AC_FINISHED_TASKS } from '../App';
import { ApiUrl, mainColor } from '../Config';
import { getToken } from './HomeScreen';
import { Category, SubCategory, Task } from './Models';
import { calcTileDimensions, Tile } from './Tile';
import { TaskSubcategoryType, TaskType } from './types';

const { width } = Dimensions.get('window');
import axios, { AxiosResponse } from 'axios';
const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: mainColor,
    },
    body: {
        backgroundColor: mainColor,
        height: Dimensions.get('window').height,
        // minHeight:  calcTileDimensions(width, 2).size * 2,
        justifyContent: "flex-start",
        flexDirection: "row",
        flexWrap: "wrap",
        marginTop: 30,
    },
});

type Props = {
    navigation: NavigationStackProp<{task: Category}>;
};

export const getStoredTasks = (then: ( tasks: {[key: string]: string}) => void) => {
    AsyncStorage.getItem(AC_FINISHED_TASKS).then(stored => {
        if (stored !== undefined && stored !== null) {
            then(JSON.parse(stored) as {[key: string]: string});
        } else {
            then({});
        }
    }).catch(reason => {
        console.log(reason);
        then({});
    })
};

const CategoriesScreen: React.FC<Props> = ({ navigation }) => {
    const mainCategory: Category = navigation.getParam('category');
    const [finishedTasks,setFinishedTasks] = useState<{[key: string]: string}>({});
    const subCategories: TaskSubcategoryType[] = mainCategory.subcategories;
    const { navigate } = navigation;
    const tileDimensions = calcTileDimensions(width, 2);
    useEffect(() => {
        getStoredTasks((tasks => setFinishedTasks(tasks)));
    }, []);
    return (
        <>
            <StatusBar barStyle={'dark-content'} backgroundColor={mainColor} />
            <SafeAreaView>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}>
                    <View style={styles.body}>
                        {
                            subCategories.map((subcategory: TaskSubcategoryType) => {
                                return (
                                <Tile
                                    key={uuid.v4().toString()}
                                    size={tileDimensions.size}
                                    margin={tileDimensions.margin}
                                    title={subcategory.description}
                                    imgSrc={`${ApiUrl}/${subcategory.cover}`}
                                    onTap={() => {
                                        getToken().then(token => {
                                            axios.get(`${ApiUrl}/api/tasks?category=${mainCategory.name}&subcategory=${subcategory.name}`, {headers: {Authorization: `Bearer ${token.access_token}`}}).then((response: AxiosResponse) =>{
                                               if (response.status < 400) {
                                                    const _tasks = response.data as TaskType[];
                                                    const tasks = _tasks.map(_task => Task.fromJSON(_task)).filter(task => task !== null).filter(t => {
                                                        if (Object.keys(finishedTasks).indexOf(t.id) < 0) {
                                                            return true;
                                                        }
                                                        const resources_id = finishedTasks[t.id];
                                                        return t.resources_id !== resources_id;
                                                    });
                                                    if (tasks.length > 0) {
                                                        navigate('SubCategory', {category: mainCategory, subcategory, tasks});
                                                    }
                                               }
                                            }).catch(reason => {
                                                console.log(reason);
                                            });
                                        });
                                        // navigate('SubCategory', {type: mainCategory.name, category});
                                        return true;
                                    }}
                                />
                            )})
                        }
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
};

export default CategoriesScreen;
