export enum TaskStatus {
    Created,
    Busy,
    Running,
    Finished,
    Ready
}
//
// export interface TaskType {
//     id: string;
//     title: string;
//     cover: string;
//     description: string;
//     resources: any;
//     status: TaskStatus;
// }
//
// export enum CategoryTaskType {
//     image,
//     sound,
//     text,
//     other,
//     binary,
//     detect,
//     multi,
//     caption,
// }
// export interface CategoryType {
//     type: CategoryTaskType;
//     title: string;
//     cover: string;
//     subcategories: CategoryType[];
//     tasks: TaskType[];
// }

export enum CategoryType {
    image,
    sound,
    text,
    other
}

export enum SubcategoryType {
    binary,
    multi,
    cation,
    detect,
}


export interface TaskSubcategoryType {
    name: SubcategoryType;
    cover: string;
    description: string;
}

export interface TaskCategoryType {
    name: CategoryType;
    cover: string;
    description: string;
    subcategories: TaskSubcategoryType[];
}

export interface TaskType {
    id: string;
    resources_id: string;
    status: TaskStatus;
    title: string;
    description: string;
    cover: string;
    category: CategoryType;
    subcategory: SubcategoryType;
    class_names: string[];
}
