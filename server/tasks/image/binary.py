import os
import shutil
import uuid
import random
import torchvision
import torch.nn as nn
import torch.optim as optim
from typing import List, Dict, Optional, Tuple, Union

from sqlalchemy.orm import Session
from torch.optim import lr_scheduler
from server import SHARED_DIR, STATIC_ROOT
from server.app.models import Task, Resource
from active_transfer import (
    Task as ATTask,
    QueryStrategy,
    ActiveTransfer,
    PhaseSize,
    weighted_dir_items,
    Phase,
)

MODEL_NAME = "model.pt"
DATA_DIR_NAME = ".data"


def get_dataset_transform(is_train: bool = False):
    """Get dataset transformation.

    :param is_train: Whether is a train dataset or not
    :return:The transform function
    """
    if is_train:
        return torchvision.transforms.Compose(
            [
                torchvision.transforms.RandomResizedCrop(224),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor(),
                torchvision.transforms.Normalize(
                    [0.485, 0.456, 0.406], [0.229, 0.224, 0.225]
                ),
            ]
        )
    return torchvision.transforms.Compose(
        [
            torchvision.transforms.Resize(256),
            torchvision.transforms.CenterCrop(224),
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize(
                [0.485, 0.456, 0.406], [0.229, 0.224, 0.225]
            ),
        ]
    )


def _labeled_sizes(task_root, class_names):
    labeled_files = [os.listdir(os.path.join(task_root, x)) for x in class_names]
    return [len(x) for x in labeled_files]


def _move_files(samples: dict, dst_root: str, src_root: str):
    for class_name, item_names in samples.items():
        src_dir = os.path.join(src_root, class_name)
        dst_dir = os.path.join(dst_root, class_name)
        if (
            os.path.exists(src_dir)
            and os.path.exists(dst_dir)
            and os.path.isdir(src_dir)
            and os.path.isdir(dst_dir)
        ):
            for _file in item_names:
                src = os.path.join(src_dir, _file)
                _extension = _file.split(".")[-1]
                dst = os.path.join(
                    dst_dir, f"{len(os.listdir(dst_dir))+1:06d}.{_extension}"
                )
                if os.path.exists(src):
                    shutil.move(src, dst)


def _check_dirs(task: Task, at: ActiveTransfer, class_names: List[str]):
    should_train = False
    task_root = os.path.realpath(os.path.join(SHARED_DIR, task.id))
    val_path = at.val_dir(True)
    test_path = at.test_dir(True)
    train_path = at.train_dir(True)
    val_current = [os.listdir(os.path.join(val_path, x)) for x in class_names]
    test_current = [os.listdir(os.path.join(test_path, x)) for x in class_names]
    train_current = [os.listdir(os.path.join(train_path, x)) for x in class_names]
    val_sizes = [len(x) for x in val_current]
    test_sizes = [len(x) for x in test_current]
    train_sizes = [len(x) for x in train_current]
    labeled_sizes = _labeled_sizes(task_root, class_names)
    labeled_min = min(labeled_sizes)
    val_total = sum(val_sizes)
    train_total = sum(train_sizes)
    test_total = sum(test_sizes)
    if val_total < task.val_count and labeled_min > val_total:
        samples = weighted_dir_items(
            task_root, count=task.val_count - val_total, names=class_names
        )
        _move_files(samples, val_path, task_root)
        labeled_sizes = _labeled_sizes(task_root, class_names)
        labeled_min = min(labeled_sizes)
    if test_total < task.test_count and labeled_min > test_total:
        samples = weighted_dir_items(
            task_root, count=task.test_count - test_total, names=class_names
        )
        _move_files(samples, test_path, task_root)
        labeled_sizes = _labeled_sizes(task_root, class_names)
        labeled_min = min(labeled_sizes)
    if train_total < task.append_count and labeled_min > train_total:
        # initial train
        samples = weighted_dir_items(
            task_root, count=task.append_count - train_total, names=class_names
        )
        _move_files(samples, train_path, task_root)
        should_train = True
    return should_train


def _prepare(at: ActiveTransfer, task: Task) -> Tuple[ActiveTransfer, any, any, any]:
    # TODO: transfer: include fine tuning (not only fixed extractor)
    for param in at.model.parameters():
        param.requires_grad = False
    num_features = at.model.fc.in_features
    at.model.fc = nn.Linear(num_features, 2)
    at.model = at.model.to(at.device)
    criterion = nn.CrossEntropyLoss()
    lr = task.optimizer_lr
    momentum = task.optimizer_momentum
    params = at.model.fc.parameters()
    optimizer = optim.SGD(params, lr=lr, momentum=momentum)
    step_size = task.scheduler_step
    scheduler = lr_scheduler.StepLR(optimizer, step_size=step_size)
    return at, criterion, optimizer, scheduler


def _move_samples_to(at: ActiveTransfer, labels: Dict[str, bool], root_dir: str):
    class_names: List[str] = at.class_names
    class_files = [len(os.listdir(os.path.join(root_dir, x))) + 1 for x in class_names]
    not_moved = {}
    limit_files = root_dir != at.train_dir(True)
    upper_limit = -1
    if limit_files:
        upper_limit = (
            at.scenario.test_size
            if root_dir == at.test_dir(True)
            else at.scenario.val_size
        )
    for image, class_value in labels.items():
        image_name = image.split("/")[-1]
        image_extension = image_name.split(".")[-1]
        if class_value is bool:
            class_value = class_names[0] if class_value else class_names[1]
        class_name = class_value
        _array_index = 0 if class_value == class_names[0] else 1
        destination_name = class_files[_array_index]
        class_files[_array_index] += 1
        destination_dir = os.path.join(root_dir, class_name)
        if not limit_files or (
            limit_files and len(os.listdir(destination_dir)) < upper_limit
        ):
            src = os.path.join(SHARED_DIR, image)
            destination_name = f"{destination_name:06d}.{image_extension}"
            dst = os.path.join(destination_dir, destination_name)
            shutil.move(src, dst)
        else:
            not_moved[image] = class_value
    return not_moved


def _samples_destination(task: Task, at: ActiveTransfer):
    val_dir = at.val_dir(True)
    val_files = sum(len(os.listdir(os.path.join(val_dir, x))) for x in at.class_names)
    if val_files < task.val_count:
        return val_dir
    else:
        test_dir = at.test_dir(True)
        test_files = sum(
            len(os.listdir(os.path.join(test_dir, x))) for x in at.class_names
        )
        if test_files < task.test_count:
            return test_dir
    return at.train_dir(True)


def _new_samples(
    task: Task,
    at: ActiveTransfer,
    db: Session,
    commits: Resource,
    mappings: Dict[str, Union[bool, str]],
):
    # TODO: also check if we should finish the task (compare accuracies in task.results?)
    root_dst_dir = _samples_destination(task, at)
    # val is full?
    not_moved = _move_samples_to(at, mappings, root_dst_dir)
    if len(not_moved) > 0:
        root_dst_dir = _samples_destination(task, at)
        # test is full ?
        not_moved = _move_samples_to(at, mappings, root_dst_dir)
    if len(not_moved) > 0:
        root_dst_dir = _samples_destination(task, at)
        # do not check for train
        _move_samples_to(at, mappings, root_dst_dir)
    db.delete(commits)
    db.commit()
    task.resources_id = str(uuid.uuid4())
    db.commit()
    db.refresh(task)


def _check_task_resources(task: Task, db: Session, at: ActiveTransfer):
    commits: Resource = db.query(Resource).filter_by(root_path=str(task.id)).first()
    if not commits:
        return False
    batch = task.append_count
    values = commits.values
    if len(values) < batch:
        return False
    current_commits = 0
    min_commits = task.min_peers_count
    mappings = {}
    accuracy = task.peer_accuracy
    for item_path, answers in values.items():
        item_answers = {x: 0 for x in task.class_names}
        for answer in answers:
            if answer is bool:
                answer = task.class_names[0] if answer else task.class_names[1]
            item_answers[answer] += 1
        if len(answers) >= min_commits:
            winner = max(item_answers.values())
            total = sum(item_answers.values())
            item_accuracy = winner / total
            if item_accuracy >= accuracy:
                mappings[item_path] = [
                    x for x, y in item_answers.items() if y == winner
                ][0]
                current_commits += 1
    samples_ready = current_commits >= min_commits
    if samples_ready:
        _new_samples(task, at, db, commits, mappings)
    return samples_ready


def enough_public_dir_items(task: Task) -> bool:
    task_public_dir = os.path.join(STATIC_ROOT, str(task.id))
    if not os.path.exists(task_public_dir):
        os.makedirs(task_public_dir, exist_ok=True)
        os.chmod(task_public_dir, 0o777)
        return False
    return len(os.listdir(task_public_dir)) >= task.append_count


def enough_sample_items(at: ActiveTransfer, task: Task) -> bool:
    _samples_dir = at.samples_dir(True)
    if not os.path.exists(_samples_dir):
        os.makedirs(_samples_dir, exist_ok=True)
        os.chmod(_samples_dir, 0o777)
        return False
    return len(os.listdir(_samples_dir)) >= task.append_count


def get_at_instance(
    task: Task, class_names: List[str], notifier: callable
) -> ActiveTransfer:
    task_root = os.path.realpath(os.path.join(SHARED_DIR, str(task.id)))
    task_data_root = os.path.realpath(os.path.join(task_root, DATA_DIR_NAME))
    if not os.path.exists(task_data_root):
        os.makedirs(task_data_root)
        os.chmod(task_data_root, 0o777)
    model = torchvision.models.resnet50(pretrained=True)
    model_path = os.path.join(task_root, MODEL_NAME)
    sizes = PhaseSize(
        {"train": task.append_count, "val": task.val_count, "test": task.test_count}
    )
    non_train_transform = get_dataset_transform()
    train_transform = get_dataset_transform(is_train=True)
    transforms: Dict[Phase, torchvision.transforms.Compose] = {
        Phase.Train: train_transform,
        Phase.Val: non_train_transform,
        Phase.Sample: non_train_transform,
        Phase.Test: non_train_transform,
    }
    at_task = ATTask(
        phase_sizes=sizes,
        append_size=task.append_count,
        category=task.category,
        subcategory=task.subcategory,
        peers=task.min_peers_count,
        strategy=QueryStrategy.from_string(task.strategy.name.lower()),
        peer_accuracy=task.peer_accuracy,
    )
    return ActiveTransfer(
        task_data_root,
        model=model,
        model_path=model_path,
        transforms=transforms,
        class_names=class_names,
        scenario=at_task,
        num_epochs=task.epochs,
        batch_size=16,
        num_workers=1,
        notifier=notifier,
    )


def _add_samples(task: Task, at: ActiveTransfer, db: Session):
    samples_dir = at.samples_dir(True)
    dst_root = os.path.join(STATIC_ROOT, str(task.id))
    os.makedirs(dst_root, exist_ok=True)
    os.chmod(dst_root, 0o777)
    current_files = os.listdir(dst_root)
    if len(current_files) < task.append_count:
        all_files = os.listdir(samples_dir)
        random.shuffle(all_files)
        for file_name in all_files[: task.append_count]:
            src = os.path.join(samples_dir, file_name)
            dst = os.path.join(dst_root, file_name)
            shutil.move(src, dst)
    else:
        _check_task_resources(task, db, at)
    return at, None, None, None, False, False


def config(
    task: Task, notifier: callable, db: Session
) -> Tuple[
    Optional[ActiveTransfer], Optional[any], Optional[any], Optional[any], bool, bool
]:
    """
    Checks:
        -  initial folders: there are not enough images in the test/val/folders
            - check the unlabeled pool folder and if we have enough samples
                move them to the public dir and ask for labeling
        -  initial train: there are enough images in the train/val/test folders
            and we do not have a trained model yet
            - we should start training (first time)
        -  retrain: there exists a trained model and we have enough new labeled samples
            - we add new samples to the train folder and should retrain the model
    :param task: The stored task
    :param notifier: The function to call on updates
    :param db: The db session
    :return: an ActiveTransfer instance, the loss criterion, the optimizer,
    the rl_scheduler, if we are ready to train and if we are ready to get new samples
    """
    should_train = False
    class_names = task.class_names
    if len(class_names) == 2:
        task_root = os.path.realpath(os.path.join(SHARED_DIR, str(task.id)))
        for x in class_names:
            os.makedirs(os.path.join(task_root, x), exist_ok=True)
        at = get_at_instance(task, class_names, notifier)
        if not at.can_train or not at.can_test:
            should_train = _check_dirs(task, at, class_names)
            if not should_train:
                should_train = at.can_train and not os.path.exists(
                    os.path.join(task_root, MODEL_NAME)
                )
            if not should_train and not at.can_train and at.have_samples:
                return _add_samples(task, at, db)
            if should_train:
                at, criterion, optimizer, scheduler = _prepare(at, task)
                return at, criterion, optimizer, scheduler, True, False
        if not should_train and at.can_train:
            should_train = _check_task_resources(task, db, at)
            if should_train and at.can_train:
                at, criterion, optimizer, scheduler = _prepare(at, task)
                return at, criterion, optimizer, scheduler, True, False
        if not should_train:
            _enough_sample_items = enough_sample_items(at, task)
            _enough_public_dir_items = enough_public_dir_items(task)
            should_sample = _enough_sample_items and not _enough_public_dir_items
            if should_sample:
                at, criterion, optimizer, scheduler = _prepare(at, task)
                return at, criterion, optimizer, scheduler, False, True
    return None, None, None, None, False, False
