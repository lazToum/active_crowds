import copy
import os
import shutil
import uuid
from datetime import datetime
from typing import List, Tuple
import torch
from celery import Celery
from active_transfer import (
    ActiveTransfer,
    Category as ATCategory,
    SubCategory as ATSubcategory,
    Phase,
)
from sqlalchemy.orm import Session

try:
    from server import SHARED_DIR, STATIC_ROOT
except ModuleNotFoundError:
    import sys

    sys.path.append(
        os.path.relpath(os.path.join(os.path.realpath(__file__), "..", "..", ".."))
    )
    from server import SHARED_DIR, STATIC_ROOT

from server.app.database import SessionLocal
from server.app import models
from server.tasks.image import image_binary_config
from server.app.schemas import TaskStatus

REDIS_PASSWORD = os.environ.get("REDIS_PASSWORD", "redis")
REDIS_PORT = os.environ.get("REDIS_PORT", "6379")
REDIS_URL = f"redis://:{REDIS_PASSWORD}@redis:{REDIS_PORT}"
BROKER_URL = REDIS_URL
CELERY_RESULT_BACKEND = REDIS_URL


def notifier(text, *_, **__):
    """
    Update function.

    Just print it here, maybe a websocket update/notification in other examples
    """
    print(text)


def make_celery():
    _celery = Celery("worker")
    _celery.conf.update(
        {
            "broker_url": BROKER_URL,
            "result_backend": CELERY_RESULT_BACKEND,
            "task_serializer": "json",
            "result_serializer": "json",
            "accept_content": ["json"],
        }
    )
    _celery.autodiscover_tasks(["tasks"], force=True)
    return _celery


celery = make_celery()


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **_):
    sender.add_periodic_task(60.0, beat.s(), name="Check tasks status")


def _task_dirs(task_id):
    resources_path = os.path.relpath(os.path.join(SHARED_DIR, task_id))
    os.makedirs(resources_path, exist_ok=True)
    os.chmod(resources_path, 0o777)
    resources_path = os.path.relpath(os.path.join(resources_path, ".data"))
    os.makedirs(resources_path, exist_ok=True)
    os.chmod(resources_path, 0o777)
    resources_path = os.path.relpath(os.path.join(STATIC_ROOT, task_id))
    os.makedirs(resources_path, exist_ok=True)
    os.chmod(resources_path, 0o777)


def _check_image_task(task: models.Task):
    try:
        task_id = str(task.id)
        _task_dirs(task_id)
    except Exception as e:
        print(e)


def _check_task(task: models.Task, db: Session):
    if task.category == ATCategory.Image and task.subcategory == ATSubcategory.Binary:
        _check_image_task(task)
        config: Tuple[ActiveTransfer, any, any, any, bool, bool] = image_binary_config(
            task, notifier, db
        )
        at = config[0]
        criterion = config[1]
        optimizer = config[2]
        scheduler = config[3]
        should_train = config[4]
        should_sample = config[5]
        if (
            at is not None
            and criterion is not None
            and optimizer is not None
            and scheduler is not None
        ):
            if at.can_train and should_train:
                return at, criterion, optimizer, scheduler, True
            if at.have_samples and should_sample:
                return at, criterion, optimizer, scheduler, False
    return None, None, None, None, False


@celery.task
def beat():
    db: Session = SessionLocal()
    tasks: List[models.Task] = db.query(models.Task).all()
    for task in tasks:
        task_status = copy.deepcopy(task.status)
        if task_status == TaskStatus.Created:
            task_status = TaskStatus.Ready
        if task.status != TaskStatus.Running and task.status != TaskStatus.Busy:
            task.status = TaskStatus.Busy
            db.commit()
            db.refresh(task)
            at, criterion, optimizer, scheduler, should_train = _check_task(task, db)
            if at is not None:
                if should_train:
                    print("starting train...")
                    return train(at, criterion, optimizer, scheduler, task, db)
                else:
                    print("getting new samples...")
                    return sample(at, task, db)
        task.status = task_status
        db.commit()
        db.refresh(task)
    db.close()


@celery.task
def train(
    at: ActiveTransfer,
    criterion: any,
    optimizer: any,
    scheduler: any,
    task: models.Task,
    db: Session,
):
    task.status = TaskStatus.Running
    db.commit()
    db.refresh(task)
    model, _, _ = at.train(criterion, optimizer, scheduler, load_if_exists=True)
    torch.save(model, at.model_path)
    os.chmod(at.model_path, 0o777)
    test_results = at.test()
    _train_dir = at.train_dir(True)
    test_results[Phase.Train.value] = sum(
        len(os.listdir(os.path.join(_train_dir, x))) for x in os.listdir(_train_dir)
    )
    if not task.results:
        task.results = {}
    task.results[str(datetime.now())] = test_results
    task.status = TaskStatus.Ready
    db.commit()
    db.refresh(task)
    db.close()


@celery.task
def sample(at: ActiveTransfer, task: models.Task, db: Session):
    task.status = TaskStatus.Busy
    db.commit()
    db.refresh(task)
    at.model = torch.load(at.model_path)
    new_samples_tuples = at.get_samples(task.append_count)
    for src, _ in new_samples_tuples:
        basename = os.path.basename(src)
        dst = os.path.join(STATIC_ROOT, str(task.id), basename)
        if os.path.exists(dst):
            new_name = f"{uuid.uuid4()}_{os.path.basename(src)}"
            dst = os.path.join(STATIC_ROOT, str(task.id), new_name)
        shutil.move(src, dst)
    task.status = TaskStatus.Ready
    db.commit()
    db.refresh(task)
    db.close()
