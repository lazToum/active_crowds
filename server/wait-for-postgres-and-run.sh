#!/usr/bin/env bash
set -e
POSTGRES_PORT=${POSTGRES_PORT:-5432}
POSTGRES_HOST=${POSTGRES_HOST:-localhost}
POSTGRES_PASSWORD=${POSTGRES_PASSWORD:-dbpassword}
POSTGRES_DB=${POSTGRES_DB:-dbname}
POSTGRES_USER=${POSTGRES_USER:-dbuser}

until PGPASSWORD="${POSTGRES_PASSWORD}" psql -h "${POSTGRES_HOST}" -d ${POSTGRES_DB} -p "${POSTGRES_PORT}" -U "${POSTGRES_USER}" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - continuing..."
if [[ ! -f ./migrations/env.py ]];then
    mkdir -p ./migrations
    chmod 0777 ./migrations
    alembic -c alembic.ini init migrations
    cp ./env.py ./migrations/env.py
else
    alembic upgrade head
fi
APP_ENV=${APP_ENV:-Dev}

if [[ ${APP_ENV} = "Dev" ]]; then
    uvicorn main:app --host 0.0.0.0 --port 8000 --reload --timeout-keep-alive 1000
else
    gunicorn main:app -b :8000 -k uvicorn.workers.UvicornWorker --keep-alive 1000 --timeout 1000
fi