import os
from fastapi import FastAPI, Depends, HTTPException, Body
from sqlalchemy.orm import Session
from starlette.middleware.cors import CORSMiddleware
from starlette.staticfiles import StaticFiles
from starlette.responses import HTMLResponse
from fastapi.openapi.docs import (
    get_swagger_ui_html,
    get_swagger_ui_oauth2_redirect_html,
)
from starlette.requests import Request
from active_transfer import Category as ATCategory, SubCategory as ATSubCategory
from oauthlib.common import generate_client_id, generate_token, always_safe
from starlette.responses import RedirectResponse

try:
    from server import APP_NAME
except ModuleNotFoundError:
    import sys

    sys.path.append(os.path.relpath(os.path.join(os.path.dirname(__file__), "..")))
    from server import APP_NAME

from server import (
    APP_DESCRIPTION,
    STATIC_ROOT,
    API_PREFIX,
    APP_VERSION,
    CONTACT_EMAIL,
    CONTACT_URL,
    TERMS_URL,
    LICENCE_NAME,
    LICENCE_URL,
)
from server.app import models, schemas, crud
from server.app.api import api as api_router, get_db, authenticated
from server.app.database import engine, SessionLocal
from server.app.swagger import get_custom_openapi

models.Base.metadata.create_all(bind=engine)

app = FastAPI(
    redoc_url=None,
    docs_url=None,
    static_directory=STATIC_ROOT,
    openapi_url="/openapi.json",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


async def swagger_ui_html(_: Request) -> HTMLResponse:
    """Change Swagger Title, self host swagger js/css"""
    return get_swagger_ui_html(
        openapi_url=app.openapi_url,
        title="ActiveCrowds",
        swagger_js_url="/static/js/swagger-ui-bundle.js",
        swagger_css_url="/static/css/swagger-ui.css",
        swagger_favicon_url="/static/favicon.ico",
    )


if app.swagger_ui_oauth2_redirect_url:  # pragma: no cover

    async def swagger_ui_redirect(_: Request) -> HTMLResponse:
        """Swagger redirect"""
        return get_swagger_ui_oauth2_redirect_html()

    app.add_route(
        app.swagger_ui_oauth2_redirect_url, swagger_ui_redirect, include_in_schema=False
    )


app.add_route("/docs", swagger_ui_html, include_in_schema=False)
app.add_route("/docs/", swagger_ui_html, include_in_schema=False)


@app.post(
    "/oauth/token",
    tags=["Auth"],
    response_model=schemas.Token,
    responses={400: {"detail": "Bad Request"}},
)
async def get_token(
    db: Session = Depends(get_db),
    client: schemas.ClientToken = Body(
        ...,
        example={
            "client_id": "client_id",
            "client_secret": "client_secret",
            "grant_type": "client_credentials",
        },
    ),
):
    if (
        client.grant_type != "client_credentials"
        and client.grant_type != "refresh_token"
    ):
        raise HTTPException(status_code=400, detail="Invalid Grant")
    _client = crud.get_client(db, client)
    if not _client:
        raise HTTPException(status_code=400, detail="Invalid Client")
    _token = None
    if client.grant_type == "client_credentials":
        _token = crud.get_token(db, _client)
    elif client.grant_type == "refresh_token" and client.refresh_token is not None:
        _token = crud.refresh_token(db, client)
    if not _token:
        raise HTTPException(status_code=400, detail="Invalid Request")
    return _token.dict()


@app.post(
    "/oauth/token/revoke",
    tags=["Auth"],
    status_code=204,
    responses={400: {"detail": "Bad Request"}},
)
async def revoke_token(
    db: Session = Depends(get_db), instance: schemas.RevokeToken = Body(...)
):
    revoked = crud.revoke_token(db, instance)
    if not revoked:
        raise HTTPException(status_code=400, detail="Invalid Request")
    return None


@app.get(
    "/oauth/token",
    tags=["Auth"],
    status_code=204,
    responses={400: {"detail": "Bad Request"}},
)
async def validate_token(_: Session = Depends(authenticated)):
    return None


def _init_categories(db: Session):
    count = db.query(models.Category).count()
    if count == 0:
        for _category in [
            ATCategory.Image,
            ATCategory.Text,
            ATCategory.Audio,
            ATCategory.Other,
        ]:
            new_category = schemas.CategoryCreate(
                name=_category,
                description=_category.name.capitalize(),
                cover=f"static/{_category}.png",
            )
            db_category = crud.add_category(db, new_category)

            if _category == ATCategory.Image:
                for _subcategory in [
                    ATSubCategory.Binary,
                    ATSubCategory.Multi,
                    ATSubCategory.Caption,
                    ATSubCategory.Detect,
                ]:
                    crud.add_subcategory(
                        db,
                        schemas.SubCategoryCreate(
                            name=_subcategory,
                            description=_subcategory.value,
                            cover=f"static/{_category}_{_subcategory.name.lower()}.png",
                        ),
                        category=db_category.name,
                    )


def _init_client(db: Session):
    count = db.query(models.Client).count()
    if count == 0:
        client_id = os.environ.get(
            "MOBILE_CLIENT_ID", generate_client_id(40, always_safe)
        )
        client_secret = os.environ.get("MOBILE_CLIENT_SECRET", generate_token(55))
        client = models.Client(client_id=client_id, client_secret=client_secret)
        db.add(client)
        db.commit()
        db.refresh(client)


@app.on_event("startup")
def _on_startup():
    """Init Categories"""
    db = SessionLocal()
    _init_client(db)
    _init_categories(db)
    db.close()


def custom_openapi():
    """Override openapi info"""
    if app.openapi_schema:  # pragma: no cover
        return app.openapi_schema
    openapi_schema = get_custom_openapi(
        title=APP_NAME,
        version=APP_VERSION,
        description=APP_DESCRIPTION,
        routes=app.routes,
        contact={"name": APP_NAME, "url": CONTACT_URL, "email": CONTACT_EMAIL},
        licence={"name": LICENCE_NAME, "url": LICENCE_URL},
        terms=TERMS_URL,
    )
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi
app.include_router(api_router, prefix=API_PREFIX, tags=["Tasks"])
app.mount("/static", StaticFiles(directory=STATIC_ROOT), name="/static")


@app.get("/", include_in_schema=False)
def root():
    return RedirectResponse("/docs/")


@app.get("/terms", include_in_schema=False)
def terms():
    return "Terms"
