from enum import Enum
from typing import List, Optional, Any
from active_transfer import (
    Category as ATCategory,
    SubCategory as ATSubCategory,
    QueryStrategy,
)
from pydantic import BaseModel


class ClientBase(BaseModel):
    client_id: str
    client_secret: str


class ClientToken(ClientBase):
    grant_type: str = "client_credentials"
    refresh_token: Optional[str] = None


class RevokeToken(ClientBase):
    token: str


class Token(BaseModel):
    token_type: str = "Bearer"
    access_token: str
    refresh_token: Optional[str] = None
    expires_in: int


class TaskStatus(str, Enum):
    Created: str = "Created"
    Busy: str = "Busy"
    Running: str = "Running"
    Finished: str = "Finished"
    Ready: str = "Ready"


class TaskCategory(str, Enum):
    Image: str = ATCategory.Image.name.lower()
    Audio: str = ATCategory.Audio.name.lower()
    Text: str = ATCategory.Text.name.lower()
    Other: str = ATCategory.Other.name.lower()


class TaskSubCategory(str, Enum):
    Binary: str = ATSubCategory.Binary.name.lower()
    Multi: str = ATSubCategory.Multi.name.lower()
    Caption: str = ATSubCategory.Caption.name.lower()
    Detect: str = ATSubCategory.Detect.name.lower()


class TaskStrategy(str, Enum):
    Entropy: str = QueryStrategy(QueryStrategy.Entropy).name.lower()
    Margin: str = QueryStrategy(QueryStrategy.Margin).name.lower()
    LeastConfidence: str = QueryStrategy(QueryStrategy.LeastConfidence).name.lower()
    Random: str = QueryStrategy(QueryStrategy.Random).name.lower()


class SubCategoryBase(BaseModel):
    pass


class SubCategoryCreate(SubCategoryBase):
    name: ATSubCategory
    cover: str
    description: str = "Description"


class SubCategory(SubCategoryCreate):
    name: str

    class Config:
        orm_mode = True


class CategoryBase(BaseModel):
    pass


class CategoryCreate(CategoryBase):
    name: ATCategory
    cover: str
    description: str = "Description"


class Category(CategoryCreate):
    subcategories: List[SubCategory] = []

    class Config:
        orm_mode = True


class TaskBase(BaseModel):
    status: TaskStatus = TaskStatus.Created


class TaskUpdate(TaskBase):
    status: TaskStatus = None
    title: Optional[str] = None
    description: Optional[str] = None
    cover: Optional[str] = None
    category: Optional[ATCategory] = None
    subcategory: Optional[TaskSubCategory] = None
    strategy: Optional[TaskStrategy] = None
    class_names: Optional[List[str]] = None
    epochs: Optional[int] = None
    optimizer_lr: Optional[float] = None
    optimizer_momentum: Optional[float] = None
    scheduler_step: Optional[int] = None
    append_count: Optional[int] = None
    val_count: Optional[int] = None
    test_count: Optional[int] = None
    min_peers_count: Optional[int] = None
    peer_accuracy: Optional[float] = None


class TaskCreate(TaskBase):
    title: str
    description: str
    cover: str
    category: ATCategory
    subcategory: TaskSubCategory
    strategy: TaskStrategy
    class_names: List[str] = []
    epochs: int = 20
    optimizer_lr: float = 0.001
    optimizer_momentum: float = 0.9
    scheduler_step: int = 7
    append_count: int = 20
    val_count: int = 100
    test_count: int = 100
    min_peers_count: int = 10
    peer_accuracy: float = 0.8


class Task(TaskCreate):
    id: str = None
    resources_id: str = None
    results: Any = None

    class Config:
        orm_mode = True


class ResourceBase(BaseModel):
    id: str = None
    root_path: str = None
    values: Any


class ResourceCreate(ResourceBase):
    pass


class Resource(ResourceBase):
    class Config:
        orm_mode = True
