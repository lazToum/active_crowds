import uuid
from datetime import datetime
from typing import List

from sqlalchemy import (
    JSON,
    Column,
    Enum,
    Text,
    ForeignKey,
    Integer,
    Float,
    String,
    DateTime,
    ARRAY,
)
from sqlalchemy.orm import relationship
from active_transfer import Category as ATCategory, SubCategory as ATSubCategory
from active_transfer.enums import QueryStrategy

from .database import Base
from .schemas import TaskStatus


class Client(Base):
    __tablename__ = "oauth2_client"

    client_id = Column(String(40), primary_key=True, unique=True)
    client_secret = Column(String(55), unique=True, index=True, nullable=False)


class Token(Base):
    __tablename__ = "oauth2_token"

    id = Column(Integer, primary_key=True, autoincrement=True)
    client_id = Column(
        String(40),
        ForeignKey("oauth2_client.client_id", ondelete="CASCADE"),
        nullable=False,
    )
    client = relationship("Client")
    # currently only bearer is supported
    token_type = Column(String(40))

    access_token = Column(String(255), unique=True)
    refresh_token = Column(String(255), unique=True)
    expires = Column(DateTime)

    def dict(self):
        now = datetime.now()
        return {
            "access_token": self.access_token,
            "refresh_token": self.refresh_token,
            "token_type": self.token_type,
            "expires_in": (self.expires - now).total_seconds(),
        }


class SubCategory(Base):
    __tablename__ = "subcategories"
    id = Column(String, primary_key=True, default=lambda: str(uuid.uuid4()))
    name = Column(Enum(ATSubCategory), primary_key=True, index=True, unique=True)
    description = Column(Text)
    cover = Column(String)
    category_name = Column(Enum(ATCategory), ForeignKey("categories.name"), index=True)
    category = relationship(
        "Category",
        primaryjoin="Category.name==SubCategory.category_name",
        remote_side="Category.name",
        uselist=False,
    )

    def dict(self):
        return {
            "name": self.name.name.lower(),
            "cover": self.cover,
            "description": self.description,
        }


class Category(Base):
    __tablename__ = "categories"

    id = Column(String, primary_key=True, default=lambda: str(uuid.uuid4()))
    name = Column(Enum(ATCategory), primary_key=True, index=True, unique=True)
    description = Column(Text)
    cover = Column(String)
    subcategories: List[SubCategory] = relationship(
        "SubCategory", back_populates="category"
    )

    def dict(self):
        return {
            "name": self.name.lower(),
            "cover": self.cover,
            "description": self.description,
            "subcategories": [x.dict() for x in self.subcategories],
        }


class Resource(Base):
    __tablename__ = "resources"
    id = Column(String, primary_key=True, default=lambda: str(uuid.uuid4()))
    root_path = Column(String)
    values = Column(JSON)


class Task(Base):
    __tablename__ = "tasks"
    id = Column(String, primary_key=True, default=lambda: str(uuid.uuid4()))
    title = Column(String, nullable=False)
    status = Column(Enum(TaskStatus), default=TaskStatus.Ready)
    description = Column(Text, nullable=False)
    cover = Column(String, nullable=False)
    resources_id = Column(String, index=True, default=lambda: str(uuid.uuid4()))
    epochs = Column(Integer, default=20)
    optimizer_lr = Column(Float, default=0.001)
    optimizer_momentum = Column(Float, default=0.9)
    scheduler_step = Column(Integer, default=7)
    strategy = Column(Enum(QueryStrategy), default=QueryStrategy.Entropy)
    category = Column(Enum(ATCategory), ForeignKey("categories.name"))
    subcategory = Column(Enum(ATSubCategory), ForeignKey("subcategories.name"))
    append_count = Column(Integer, default=20)
    val_count = Column(Integer, default=100)
    test_count = Column(Integer, default=100)
    min_peers_count = Column(Integer, default=10)
    peer_accuracy = Column(Float, default=0.8)
    class_names = Column(ARRAY(String), default=[])
    results = Column(JSON)

    def dict(self):
        return {
            "id": str(self.id),
            "title": self.title,
            "category": self.category.value,
            "subcategory": self.subcategory.name.lower(),
            "status": self.status,
            "class_names": self.class_names,
            "description": self.description,
            "cover": self.cover,
            "resources_id": self.resources_id,
            "strategy": self.strategy.name.lower(),
            "append_count": self.append_count,
            "val_count": self.val_count,
            "test_count": self.test_count,
            "min_peers_count": self.min_peers_count,
            "peer_accuracy": self.peer_accuracy,
        }
