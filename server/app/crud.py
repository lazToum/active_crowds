import os
import copy
import uuid
from datetime import datetime, timedelta
from typing import Optional
from oauthlib.common import generate_token
from sqlalchemy.orm import Session
from active_transfer import (
    Category as ATCategory,
    SubCategory as ATSubCategory,
    QueryStrategy,
)

from . import models, schemas
from .. import STATIC_ROOT


def get_client(db: Session, client: schemas.ClientToken):
    return (
        db.query(models.Client)
        .filter(
            models.Client.client_id == client.client_id,
            models.Client.client_secret == client.client_secret,
        )
        .first()
    )


def get_token(db: Session, client: models.Client):
    now = datetime.now()
    token = models.Token(
        token_type="Bearer",
        client_id=client.client_id,
        access_token=generate_token(55),
        refresh_token=generate_token(55),
        expires=now + timedelta(days=30),
    )
    db.add(token)
    db.commit()
    db.refresh(token)
    return token


def refresh_token(db: Session, client: schemas.ClientToken):
    existing: models.Token = db.query(models.Token).filter_by(
        client_id=client.client_id, refresh_token=client.refresh_token
    ).first()
    now = datetime.now()
    if not existing:
        return None
    existing.access_token = generate_token(55)
    existing.refresh_token = generate_token(55)
    existing.expires = now + timedelta(days=30)
    db.commit()
    db.refresh(existing)
    return existing


def revoke_token(db: Session, instance: schemas.RevokeToken):
    existing: models.Token = db.query(models.Token).filter_by(
        client_id=instance.client_id, access_token=instance.token
    ).first()
    if not existing:
        return False
    db.delete(existing)
    db.commit()
    return True


def get_category_by_id(db: Session, category_id: str):
    return db.query(models.Category).filter(models.Category.id == category_id).first()


def get_category_by_name(db: Session, category_name: str):
    category = (
        db.query(models.Category).filter(models.Category.name == category_name).first()
    )
    if category:
        return category
    return None


def get_subcategories(db: Session, category_name: str):
    category = (
        db.query(models.Category).filter(models.Category.name == category_name).first()
    )
    if category:
        return category.subcategories
    return []


def get_categories(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Category).offset(skip).limit(limit).all()


def add_category(db: Session, category: schemas.CategoryCreate):
    db_item = models.Category(**category.dict())
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item


def add_subcategory(
    db: Session, subcategory: schemas.SubCategoryCreate, category: ATCategory
):
    db_item = models.SubCategory(**subcategory.dict(), category_name=category)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item


def get_tasks(
    db: Session,
    category: Optional[ATCategory] = None,
    subcategory: Optional[ATSubCategory] = None,
):
    if category is not None:
        if subcategory is not None:
            return (
                db.query(models.Task)
                .filter(
                    models.Task.category == category,
                    models.Task.subcategory == subcategory,
                )
                .all()
            )
        return db.query(models.Task).filter(models.Task.category == category).all()
    return db.query(models.Task).all()


def add_task(db: Session, task: schemas.TaskCreate):
    task_dict = task.dict()
    task_dict["resources_id"] = str(uuid.uuid4())
    task_dict["subcategory"] = ATSubCategory.from_string(task.subcategory)
    task_dict["strategy"] = QueryStrategy.from_string(task.strategy)
    db_item = models.Task(**task_dict)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item


def get_task_by_id(db: Session, task_id: str):
    return db.query(models.Task).filter(models.Task.id == task_id).first()


def get_task_answers(db: Session, task_id: str):
    entry: Optional[models.Resource] = db.query(models.Resource).filter(
        models.Resource.root_path == task_id
    ).first()
    if entry is not None:
        return entry.values
    return {}


def update_task_by_id(db: Session, task_id: str, task: schemas.TaskUpdate):
    existing: Optional[models.Task] = get_task_by_id(db, task_id)
    if existing:
        updates = 0
        for field, value in task.dict().items():
            if value is not None:
                _value = value
                if field in ["category", "subcategory", "strategy"]:
                    _value = value.name
                setattr(existing, field, _value)
                updates += 1
        if updates > 0:
            db.commit()
            db.refresh(existing)
        return existing
    return None


def delete_task_by_id(db: Session, task_id: str):
    task = get_task_by_id(db, task_id)
    if task:
        db.delete(task)
        db.commit()
        return True
    return False


def save_task_answers(db: Session, task_id: str, answers: dict):
    task: Optional[models.Task] = get_task_by_id(db, task_id)
    if not task:
        return False
    existing: Optional[models.Resource] = db.query(models.Resource).filter_by(
        root_path=task_id
    ).first()
    existing_values = copy.deepcopy(existing.values) if existing else {}
    for file_path, value in answers.items():
        _path: str = file_path
        if (
            not _path.startswith(f"static/{task_id}")
            and task.category == ATCategory.Image
            or task.category == ATCategory.Audio
        ):
            _path = f"static/{task_id}/{file_path}"
        if os.path.exists(os.path.join(STATIC_ROOT, "..", _path)):
            if existing_values.get(_path, None) is None:
                existing_values[_path] = []
            existing_values[_path].append(value)
    if not existing:
        existing = models.Resource(root_path=task_id, values=existing_values)
        db.add(existing)
    else:
        existing.values = existing_values
    db.commit()
    db.refresh(existing)
    return True
