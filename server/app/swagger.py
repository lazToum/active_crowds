"""
coding=utf-8
"""
from typing import Dict, Sequence
from fastapi import routing
from fastapi.encoders import jsonable_encoder
from fastapi.openapi.models import OpenAPI, Contact, License, Info
from fastapi.openapi.utils import get_openapi_path
from fastapi.utils import get_flat_models_from_routes, get_model_definitions
from pydantic.schema import get_model_name_map
from starlette.routing import BaseRoute


def api_contact(contact_dict: dict, info_dict: dict) -> dict:
    """Contact in api info"""
    _contact: Contact = Contact(**contact_dict)
    if _contact and (_contact.name or _contact.url or _contact.email):
        info_dict["contact"] = {}
        for _key in _contact.dict().keys():
            _value = getattr(_contact, _key, None)
            if _value is not None:
                info_dict["contact"][_key] = _value
    return info_dict


def api_license(license_dict: dict, info_dict: dict) -> dict:
    """License in api info"""
    licence: License = License(**license_dict)
    if licence and (licence.name or licence.url):
        info_dict["license"] = {}
        for _key in licence.dict().keys():
            _value = getattr(licence, _key, None)
            if _value is not None:
                info_dict["license"][_key] = _value
    return info_dict


def open_api_info(
    title,
    version,
    description=None,
    contact: dict = None,
    licence: dict = None,
    **kwargs
) -> Info:
    """OpenAPI Info"""
    info = {"title": title, "version": version}
    terms: str = kwargs.get("terms", None)
    if terms:
        info["termsOfService"] = terms
    if description:
        info["description"] = description
    info = api_contact(contact, info)
    info = api_license(licence, info)
    return Info(**info)


def _update_route(
    route, path, paths, path_definitions, components, definitions, **kwargs
):
    """Update OpenApi route"""
    openapi_prefix = kwargs.get("openapi_prefix")
    security_schemes = kwargs.get("security_schemes")
    if path:
        paths.setdefault(openapi_prefix + route.path_format, {}).update(path)
    if security_schemes:  # pragma: no cover
        components.setdefault("securitySchemes", {}).update(security_schemes)
    if path_definitions:
        definitions.update(path_definitions)
    return components, definitions, paths


def api_components(
    routes, paths, definitions, model_name_map, openapi_prefix
) -> Dict[str, Dict]:
    """Api Components and paths"""
    components: Dict[str, Dict] = {}
    for _route in routes:
        if isinstance(_route, routing.APIRoute):
            route: routing.APIRoute = _route
            result = get_openapi_path(route=route, model_name_map=model_name_map)
            if result:
                path, security_schemes, path_definitions = result
                components, definitions, paths = _update_route(
                    route,
                    path,
                    paths,
                    path_definitions,
                    components,
                    definitions,
                    openapi_prefix=openapi_prefix,
                    security_schemes=security_schemes,
                )
    if definitions:
        components["schemas"] = {k: definitions[k] for k in sorted(definitions)}
    return components


def get_custom_openapi(
    *,
    title: str,
    version: str,
    description: str = None,
    routes: Sequence[BaseRoute],
    contact: dict = None,
    licence: dict = None,
    **kwargs
) -> Dict:
    """Custom openapi info"""
    openapi_prefix: str = kwargs.get("openapi_prefix", "")
    openapi_version: str = kwargs.get("openapi_version", "3.0.2")
    output = {
        "openapi": openapi_version,
        "info": open_api_info(
            title=title,
            version=version,
            description=description,
            contact=contact,
            licence=licence,
            **kwargs
        ).dict(),
    }
    paths: Dict[str, Dict] = {}
    flat_models = get_flat_models_from_routes(routes)
    model_name_map = get_model_name_map(flat_models)
    definitions = get_model_definitions(
        flat_models=flat_models, model_name_map=model_name_map
    )
    components = api_components(
        routes=routes,
        paths=paths,
        definitions=definitions,
        model_name_map=model_name_map,
        openapi_prefix=openapi_prefix,
    )
    if components:
        output["components"] = components
    output["paths"] = paths
    return jsonable_encoder(OpenAPI(**output), by_alias=True, include_none=False)
