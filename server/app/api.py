import os
import random
import shutil
from datetime import datetime
from fastapi import Body, APIRouter, HTTPException, Depends, UploadFile, File
from typing import List, Optional, Dict
from starlette.status import HTTP_401_UNAUTHORIZED

from server.app.database import SessionLocal
from server import SHARED_DIR, STATIC_ROOT
from server.app import schemas, crud, models
from sqlalchemy.orm import Session
from active_transfer import Category as ATCategory, SubCategory as ATSubCategory, weighted_sizes
from fastapi.security.http import HTTPBearer, HTTPAuthorizationCredentials

api = APIRouter()

api_key_bearer = HTTPBearer()

task_example = {
    "title": "string",
    "description": "task description",
    "cover": "static/string.png",
    "category": "image",
    "subcategory": "binary",
    "class_names": ["positive", "negative"],
    "strategy": "entropy",
    "append_count": 30,
    "val_count": 100,
    "test_count": 100,
    "min_peers_count": 10,
    "peer_accuracy": 0.8,
}


# Dependency
def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        if db:
            db.close()


def _check_task(task_id: str):
    print(task_id)


def celery_on_message(body):
    print(body)


def background_on_message(task):
    print(task.get(on_message=celery_on_message, propagate=False))


def not_found(thing: str):
    return f"{thing} Not Found"


async def authenticated(
    auth: HTTPAuthorizationCredentials = Depends(api_key_bearer), db: Session = Depends(get_db)
):
    if auth.scheme != "Bearer":
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="Invalid Scheme")
    token = auth.credentials
    _stored: models.Token = db.query(models.Token).filter_by(access_token=token).first()
    if not _stored:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="Invalid token")
    expires = _stored.expires
    now = datetime.now()
    if expires < now:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="Token has expired")
    return db


@api.head("/ping", include_in_schema=False, status_code=204)
async def ping_head():  # pragma: no cover
    """Health check"""
    return None


@api.get("/ping", tags=["other"])
async def ping():
    """Check instance availability"""
    return "pong"


@api.get("/categories", response_model=List[schemas.Category])
async def get_categories(db: Session = Depends(authenticated)):
    categories = crud.get_categories(db)
    return [x.dict() for x in categories]


@api.get("/categories/{category}", response_model=schemas.SubCategory)
async def get_category(category: ATCategory, db: Session = Depends(authenticated)):
    category = crud.get_category_by_name(db, category)
    return category.dict()


@api.get("/categories/{category}/subcategories", response_model=List[schemas.SubCategory])
async def get_subcategories(category: ATCategory, db: Session = Depends(authenticated)):
    subcategories = crud.get_subcategories(db, category)
    return [x.dict() for x in subcategories]


@api.get("/tasks", response_model=List[schemas.Task])
async def get_tasks(
    category: Optional[ATCategory] = None,
    subcategory: Optional[schemas.TaskSubCategory] = None,
    db: Session = Depends(authenticated),
):
    tasks = crud.get_tasks(
        db,
        category=category,
        subcategory=ATSubCategory.from_string(subcategory) if subcategory is not None else None,
    )
    return [x.dict() for x in tasks]


@api.post("/tasks", response_model=schemas.Task)
async def create_new_task(
    task: schemas.TaskCreate = Body(..., example=task_example),
    db: Session = Depends(authenticated),
):
    if task.subcategory == schemas.TaskSubCategory.Binary and len(task.class_names) != 2:
        raise HTTPException(
            status_code=400, detail="Invalid task subcategory, class_names combination"
        )
    new_task: schemas.Task = crud.add_task(db, task)
    task_id = str(new_task.id)
    task_dir = os.path.join(SHARED_DIR, task_id)
    os.makedirs(task_dir, exist_ok=True)
    os.chmod(task_dir, 0o777)
    task_public = os.path.join(STATIC_ROOT, task_id)
    os.makedirs(task_public, exist_ok=True)
    os.chmod(task_public, 0o777)
    _dict = new_task.dict()
    _dict["subcategory"] = new_task.subcategory.name.lower()
    _dict["strategy"] = new_task.strategy.name.lower()
    return _dict


@api.get(
    "/tasks/{task_id}", response_model=schemas.Task, responses={404: {"detail": not_found("Task")}}
)
async def get_task_details(task_id: str, db: Session = Depends(authenticated)):
    task = crud.get_task_by_id(db, task_id)
    if not task:
        raise HTTPException(status_code=404, detail=not_found("Task"))
    return task.dict()


@api.delete("/tasks/{task_id}", status_code=204, responses={404: {"detail": not_found("Task")}})
async def delete_task_by_id(task_id: str, db: Session = Depends(authenticated)):
    if not crud.delete_task_by_id(db, task_id):
        raise HTTPException(status_code=404, detail=not_found("Task"))
    task_root_dir = os.path.relpath(os.path.join(SHARED_DIR, task_id))
    if os.path.exists(task_root_dir):
        shutil.rmtree(task_root_dir)
    task_public_dir = os.path.relpath(os.path.join(STATIC_ROOT, task_id))
    if os.path.exists(task_public_dir):
        shutil.rmtree(task_public_dir)
    return None


@api.patch(
    "/tasks/{task_id}", response_model=schemas.Task, responses={404: {"detail": not_found("Task")}}
)
async def update_task_by_id(
    task_id: str,
    task: schemas.TaskUpdate = Body(..., example=task_example),
    db: Session = Depends(authenticated),
):
    if task.subcategory == schemas.TaskSubCategory.Binary and len(task.class_names) != 2:
        raise HTTPException(
            status_code=400, detail="Invalid task subcategory, class_names combination"
        )
    updated = crud.update_task_by_id(db, task_id=task_id, task=task)
    if not updated:
        raise HTTPException(status_code=404, detail=not_found("Task"))
    return updated.dict()


@api.post("/upload")
async def create_upload_file(
    task_id: Optional[str] = None,
    path: Optional[str] = None,
    files: List[UploadFile] = File(...),
    _: Session = Depends(authenticated),
):
    print(task_id, path)
    return {"filenames": [file.filename for file in files]}


@api.get(
    "/tasks/{task_id}/resources",
    response_model=List[str],
    responses={404: {"detail": not_found("Task")}},
)
async def get_task_resources(task_id: str, db: Session = Depends(authenticated)):
    task: models.Task = crud.get_task_by_id(db, task_id=task_id)
    if not task:
        raise HTTPException(status_code=404, detail=not_found("Task"))
    resources = []
    resources_path = os.path.relpath(os.path.join(STATIC_ROOT, task_id))
    if not os.path.exists(resources_path):
        os.makedirs(resources_path)
        os.chmod(resources_path, 0o777)
    if task.category == ATCategory.Image:
        resources = [f"static/{task_id}/{x}" for x in sorted(os.listdir(resources_path))]
    return resources


@api.post(
    "/tasks/{task_id}/values", status_code=201, responses={404: {"detail": not_found("Task")}}
)
async def commit_task_values(task_id: str, values: Dict, db: Session = Depends(authenticated)):
    _saved = crud.save_task_answers(db, task_id, values)
    if not _saved:
        raise HTTPException(status_code=404, detail=not_found("Task"))
    return None


@api.get("/tasks/{task_id}/values", responses={404: {"detail": not_found("Task")}})
async def get_task_values(task_id: str, db: Session = Depends(authenticated)):
    return crud.get_task_answers(db, task_id)


@api.get("/results/{task_id}", responses={404: {"detail": not_found("Task")}})
async def get_task_results(task_id: str, db: Session = Depends(authenticated)):
    task: models.Task = crud.get_task_by_id(db, task_id)
    if task:
        return task.results
    raise HTTPException(status_code=404, detail=not_found("Task"))


def _simulate_answers(task: models.Task, db: Session):
    task_id = str(task.id)
    task_public_dir = os.path.join(STATIC_ROOT, task_id)
    if os.path.exists(task_public_dir):
        answers = {}
        positive = task.class_names[0]
        negative = task.class_names[1]
        for file_name in os.listdir(task_public_dir):
            if negative in file_name:
                answers[f"static/{task_id}/{file_name}"] = negative
            elif positive in file_name:
                answers[f"static/{task_id}/{file_name}"] = positive
        if len(answers) > 0:
            for _ in range(task.min_peers_count):
                crud.save_task_answers(db, task_id, answers)


@api.get("/debug", status_code=201, include_in_schema=os.environ.get("APP_ENV", "") != "Prod")
async def debug(task_id: str, db: Session = Depends(authenticated)):
    task: models.Task = crud.get_task_by_id(db, task_id)
    if task and task.category == ATCategory.Image and task.subcategory == ATSubCategory.Binary:
        _simulate_answers(task, db)
        return None
    raise HTTPException(status_code=404, detail=not_found("Task"))


def _submit_answers(file_names, class_names, destination_root, source_root):
    positive = class_names[0]
    negative = class_names[1]
    positives = []
    negatives = []
    for file_name in file_names:
        if negative in file_name:
            negatives.append(file_name)
        else:
            positives.append(file_name)
    for class_name, files_list in [(negative, negatives), (positive, positives)]:
        dst_dir = os.path.join(destination_root, class_name)
        for _item in files_list:
            src = os.path.join(source_root, _item)
            _name, _ext = os.path.splitext(_item)
            _dst_name = f"{len(os.listdir(dst_dir)) + 1:06d}{_ext}"
            dst = os.path.join(dst_dir, _dst_name)
            shutil.move(src, dst)


def _check_existing_files(current, goal, samples_dir, weights, big, small, class_names, dst_root):
    if current < goal:
        diff = goal - current
        if len(os.listdir(samples_dir)) >= diff:
            sizes = weighted_sizes(diff, weights)
            items = big[: sizes[0]]
            _submit_answers(items, class_names, dst_root, samples_dir)
            items = small[: sizes[1]]
            _submit_answers(items, class_names, dst_root, samples_dir)


def _simulate_answers_till_train(task: models.Task, db: Session):
    task_dir = os.path.join(SHARED_DIR, str(task.id))
    task_data_dir = os.path.join(task_dir, ".data")
    samples_dir = os.path.join(task_data_dir, "pool")
    val_dir = os.path.join(task_data_dir, "val")
    test_dir = os.path.join(task_data_dir, "test")
    val_goal = task.val_count
    val_current = sum(len(os.listdir(os.path.join(val_dir, x))) for x in task.class_names)
    test_goal = task.test_count
    test_current = sum(len(os.listdir(os.path.join(test_dir, x))) for x in task.class_names)
    negative = [x for x in os.listdir(samples_dir) if task.class_names[1] in x]
    positive = [x for x in os.listdir(samples_dir) if x not in negative]
    negative_count = len(negative)
    positive_count = len(positive)
    ordered = [max(negative_count, positive_count), min(negative_count, positive_count)]
    big = negative
    small = positive
    if negative_count < positive_count:
        big = positive
        small = negative
    random.shuffle(big)
    random.shuffle(small)
    for current, goal, root in [
        (val_current, val_goal, val_dir),
        (test_current, test_goal, test_dir),
    ]:
        _check_existing_files(
            current, goal, samples_dir, ordered, big, small, task.class_names, root
        )


@api.get("/superdebug", status_code=201, include_in_schema=False)
async def debug(task_id: str, db: Session = Depends(authenticated)):
    task: models.Task = crud.get_task_by_id(db, task_id)
    if task and task.category == ATCategory.Image and task.subcategory == ATSubCategory.Binary:
        _simulate_answers_till_train(task, db)
        return None
    raise HTTPException(status_code=404, detail=not_found("Task"))


#     task_id: str, background_task: BackgroundTasks, _: Session = Depends(authenticated)
# ):
#     task = celery.send_task("tasks.process_data", args=[task_id])
#     background_task.add_task(background_on_message, task)
#     return {"message": "Word received"}
