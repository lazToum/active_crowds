"""
coding=utf-8
"""
import os

_url = f"https://{os.environ.get('DOMAIN_NAME', 'example.com')}"
SHARED_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "shared")
STATIC_ROOT = os.path.join(SHARED_DIR, "static")
CACHE_DIR = os.path.join(SHARED_DIR, "cache")
REDIS_URL = "redis://redis:6379/0"
APP_NAME = "ActiveCrowds"
APP_DESCRIPTION = "ActiveCrowds API DOCS"
APP_VERSION = "0.1.3"
API_PREFIX = "/api"
CONTACT_URL = _url
CONTACT_EMAIL = "laztoum@protonmail.com"
TERMS_URL = f"{_url}/terms"
LICENCE_NAME = "Apache 2.0"
LICENCE_URL = "http://www.apache.org/licenses/LICENSE-2.0.html"
__all__ = [
    SHARED_DIR,
    STATIC_ROOT,
    CACHE_DIR,
    REDIS_URL,
    APP_NAME,
    APP_DESCRIPTION,
    APP_VERSION,
    API_PREFIX,
    CONTACT_URL,
    CONTACT_EMAIL,
    TERMS_URL,
    LICENCE_NAME,
    LICENCE_URL,
]
